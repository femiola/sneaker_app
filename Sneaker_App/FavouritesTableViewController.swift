//
//  FavouritesTableViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 12/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

class ShowMyFavouritShoe: UITableViewCell {
    
    @IBOutlet weak var shoeLabel: UILabel!
    @IBOutlet weak var shoeMyImage: UIImageView!
}

class FavouritesTableViewController: UITableViewController {
    
    var selectItem: Favourite!
    
    
    lazy var fetchedResultsController: NSFetchedResultsController = { () -> NSFetchedResultsController<Favourite> in
        
        let fetchRequest = Favourite.fetchRequest() as NSFetchRequest<Favourite>
        
        let imageSort = NSSortDescriptor(key: "image", ascending: true)
        let idSort = NSSortDescriptor(key: "id", ascending: true)
        let nameSort = NSSortDescriptor(key: "name", ascending: true)
        
        fetchRequest.sortDescriptors = [nameSort, imageSort]
        
        fetchRequest.fetchBatchSize = 15
        
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true), NSSortDescriptor(key: "image", ascending: true),NSSortDescriptor(key: "id", ascending: true)]
        
         // Initialize Fetched Results Controller
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: CoreDataContoller.shared.mainContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
        // Configure Fetched Results Controller
        frc.delegate = self
        
        return frc
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableView.reloadData()
        
        tableView.tableFooterView = UIView()
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return fetchedResultsController.sections?[section].numberOfObjects ?? 0
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myFav = tableView.dequeueReusableCell(withIdentifier: "allMyFavouriteShoes", for: indexPath)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "allMyFavouriteShoes", for: indexPath) as? ShowMyFavouritShoe else{ return myFav}
        
        
        let searchRepo = fetchedResultsController.object(at: indexPath)
        cell.shoeLabel.text = searchRepo.name
        cell.shoeMyImage.sd_setImage(with: URL(string: searchRepo.image ?? ""), placeholderImage: #imageLiteral(resourceName: "HyperLogo"))
        
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete Favourite") { (action, indexPath) in
            let objectToRemove = self.fetchedResultsController.object(at: indexPath)
            self.fetchedResultsController.managedObjectContext.delete(objectToRemove)
            CoreDataContoller.shared.saveContext()
        }
        return [deleteAction]
    }
    
}

extension FavouritesTableViewController : NSFetchedResultsControllerDelegate{
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                tableView.moveRow(at: indexPath, to: newIndexPath)
            }
        @unknown default:
            fatalError("@unknown default")
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        @unknown default:
            fatalError("@unknown default")
        }
    }
}



