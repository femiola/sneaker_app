//
//  Notification.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 15/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class NotificationPublisher: NSObject{
    
    // Setting the notification function
    func sendNotification(title: String, subtitle: String, body: String, badge : Int){
        
        // Getting the date and trying to find out the actual time
        let formatter = DateFormatter()
        let date = Date()
        formatter.dateFormat = "HH:mm:ss"
        let timeNow = formatter.string(from: date)
        print("Timer\(timeNow)")
        let timeArray = timeNow.components(separatedBy: ":")
        
        
        let notificationContent = UNMutableNotificationContent()
        
        
        notificationContent.title = title
        notificationContent.subtitle = subtitle
        notificationContent.body = body
        notificationContent.sound = UNNotificationSound.default
        
        
        let calendar = Calendar.current
        var components = DateComponents()
        components.day = calendar.component(.day, from: Date())
        components.month = calendar.component(.month, from: Date())
        print("Time: \(components)")
        components.year = calendar.component(.year, from: Date())
        if(timeArray.count == 3){
            components.hour = Int(timeArray[0])
            components.minute = Int(timeArray[1])! + 1
            components.second = Int(timeArray[2])! + 10
        }
        else{
            print("Time array Error")
        }
        let newDate = calendar.date(from: components)!
        let dateComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: newDate)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        print("DATERR: \(dateComponents)")
        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString, content: notificationContent, trigger: trigger)
        
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
        // Adding the badge up when you get a notification
        if  badge == badge{
            var currentBadgeCount = UIApplication.shared.applicationIconBadgeNumber
            currentBadgeCount += badge
            notificationContent.badge = NSNumber(integerLiteral: currentBadgeCount)
        }
        
        UNUserNotificationCenter.current().delegate = self
        
        
        UNUserNotificationCenter.current().add(request){
            error in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
}

extension NotificationPublisher: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("The notification is about to be presented")
        completionHandler([.badge, .sound, .alert])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let identifer = response.actionIdentifier
        
        switch identifer{
        case UNNotificationDismissActionIdentifier:
            print("The notification was dismissed")
            completionHandler()
        case UNNotificationDefaultActionIdentifier:
            print("The user opened the app from the notification")
        default:
            print("the default case was called")
            completionHandler()
        }
    }
    
}





