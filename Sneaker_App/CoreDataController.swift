//
//  CoreDataController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 13/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation
import CoreData

class CoreDataContoller{

static let shared = CoreDataContoller()

private init(){}

lazy var persistentContainer: NSPersistentContainer = {
       /*
        The persistent container for the application. This implementation
        creates and returns a container, having loaded the store for the
        application to it. This property is optional since there are legitimate
        error conditions that could cause the creation of the store to fail.
       */
       let container = NSPersistentContainer(name: "Sneaker_App")
       container.loadPersistentStores(completionHandler: { (storeDescription, error) in
           if let error = error as NSError? {
               // Replace this implementation with code to handle the error appropriately.
               // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
               /*
                Typical reasons for an error here include:
                * The parent directory does not exist, cannot be created, or disallows writing.
                * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                * The device is out of space.
                * The store could not be migrated to the current model version.
                Check the error message to determine what the actual problem was.
                */
               fatalError("Unresolved error \(error), \(error.userInfo)")
           }
       })
       return container
   }()

   // MARK: - Core Data Saving support

   func saveContext () {
       let context = persistentContainer.viewContext
       if context.hasChanges {
           do {
               try context.save()
           } catch {
               // Replace this implementation with code to handle the error appropriately.
               // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
               let nserror = error as NSError
               fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
           }
       }
   }
    //Testing
     func parse(myitems: [Result], completion: @escaping () -> Void) {
            
            let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            privateContext.parent = mainContext
            
            let shoeFetchRequest: NSFetchRequest<Favourite> = Favourite.fetchRequest()
           
            
            do {
                
                for mySneakerDB in myitems {
                    
                    shoeFetchRequest.predicate = NSPredicate(format: "uniqueID = \(mySneakerDB.id)")
                    let foundItems = try privateContext.fetch(shoeFetchRequest)
                    
                    let currentRepository = foundItems.first ?? Favourite(context: privateContext)
                    
                    // Set the Unique ID
                    currentRepository.id = mySneakerDB.id
                    currentRepository.name = mySneakerDB.title
                    currentRepository.image = mySneakerDB.media.imageURL
                    
   
                }
                
                try privateContext.save()
                
                completion()
                
            } catch {
                print(error)
            }
            
        }
        
        var mainContext:NSManagedObjectContext {
            return persistentContainer.viewContext
        }
        
    }


