//
//  Sneaker_AppTests.swift
//  Sneaker_AppTests
//
//  Created by Femi Oladiji on 04/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import XCTest
import FirebaseAuth

@testable import Sneaker_App

class Sneaker_AppTests: XCTestCase {
    
    var sut: ViewController!
    var collectionView: shoeCollection!
    var searchBrand: SearchBrandTableViewController!
    var model: ShoesModel?
    
    var sessionUnderTest: URLSession?
    
    override func setUp() {
        sut = ViewController()
        collectionView = shoeCollection()
        model = ShoesModel()
        
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)
        
        searchBrand = SearchBrandTableViewController()
        
    }
    
    
    override func tearDown() {
        sut = nil
        sessionUnderTest = nil
        //searchBrand = nil
        model = nil
        super.tearDown()
    }
    
    func testIfLoginButtonHasActionAssigned() {
        
        //Check if Controller has UIButton property
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "homePage") as? ViewController
        sut.loadViewIfNeeded()
        
        let loginButton: UIButton = sut.loginButton
        XCTAssertNotNil(loginButton, "View Controller does not have UIButton property")
    }
    
    func testIfSignUpButtonHasActionAssigned() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        sut = storyboard.instantiateViewController(withIdentifier: "homePage") as? ViewController
        sut.loadViewIfNeeded()
        //Check if Controller has UIButton property
        let signButton: UIButton = sut.signUpButton
        XCTAssertNotNil(signButton, "View Controller does not have UIButton property")
    }
    

    
    func test_signUp_fields_checkWhenFieldsAreEmpty() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "signUpPage") as! SignUpViewController
        vc.loadViewIfNeeded()
        
        vc.firstNameTextField.text = ""
        vc.lastNameTextField.text = ""
        vc.emailTextField.text = ""
        vc.passwordTextField.text = ""
        
        vc.signUpPressed()
        XCTAssertFalse(vc.errorLabel!.isHidden)
        XCTAssertEqual("Please fill in all fields", vc.errorLabel!.text!)
        
    }
    
    func test_signUp_fields_firstName_butOtherFieldsAreEmpty() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "signUpPage") as! SignUpViewController
        vc.loadViewIfNeeded()
        
        vc.firstNameTextField.text = "demi"
        vc.lastNameTextField.text = ""
        vc.emailTextField.text = ""
        vc.passwordTextField.text = ""
        
        vc.signUpPressed()
        XCTAssertFalse(vc.errorLabel!.isHidden)
        XCTAssertEqual("Please fill in all fields", vc.errorLabel!.text!)
        
    }
    func test_signUp_fields_LastName_butOtherFieldsAreEmpty() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "signUpPage") as! SignUpViewController
        vc.loadViewIfNeeded()
        
        vc.firstNameTextField.text = "demi"
        vc.lastNameTextField.text = "king"
        vc.emailTextField.text = ""
        vc.passwordTextField.text = ""
        
        vc.signUpPressed()
        XCTAssertFalse(vc.errorLabel!.isHidden)
        XCTAssertEqual("Please fill in all fields", vc.errorLabel!.text!)
        
    }
    func test_signUp_fields_email_butOtherFieldsAreEmpty() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "signUpPage") as! SignUpViewController
        vc.loadViewIfNeeded()
        
        vc.firstNameTextField.text = "demi"
        vc.lastNameTextField.text = "king"
        vc.emailTextField.text = "dk@live.com"
        vc.passwordTextField.text = ""
        
        vc.signUpPressed()
        XCTAssertFalse(vc.errorLabel!.isHidden)
        XCTAssertEqual("Please fill in all fields", vc.errorLabel!.text!)
        
    }
    func test_signUp_fields_password_butOtherFieldsAreEmpty() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "signUpPage") as! SignUpViewController
        vc.loadViewIfNeeded()
        
        vc.firstNameTextField.text = ""
        vc.lastNameTextField.text = ""
        vc.emailTextField.text = ""
        vc.passwordTextField.text = "iamAKing"
        
        vc.signUpPressed()
        XCTAssertFalse(vc.errorLabel!.isHidden)
        XCTAssertEqual("Please fill in all fields", vc.errorLabel!.text!)
        
    }
    
    
    func test_signUp_fields_CheckPasswordValidation() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "signUpPage") as! SignUpViewController
        vc.loadViewIfNeeded()
        
        vc.firstNameTextField.text = "demi"
        vc.lastNameTextField.text = "king"
        vc.emailTextField.text = "dk@live.com"
        vc.passwordTextField.text = "iamAKing"
        
        vc.signUpPressed()
        XCTAssertFalse(vc.errorLabel!.isHidden)
        XCTAssertEqual("Please make sure your password is at least 8 characters, contains a special character and a number", vc.errorLabel!.text!)
        
    }
    
    
    func testCallToSneakersCompletes() {
        // given
        let url =
            URL(string: "https://api.thesneakerdatabase.com/v1/sneakers?limit=100")
        let promise = expectation(description: "Completion handler invoked")
        var statusCode: Int?
        var responseError: Error?
        
        // when
        let dataTask = sessionUnderTest?.dataTask(with: url!) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            promise.fulfill()
        }
        dataTask?.resume()
        wait(for: [promise], timeout: 5)
        
        // then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }
    
    func testCallTosearchAllBrands() {
        // given
        let url =
            URL(string: "https://api.thesneakerdatabase.com/v1/sneakers?limit=100&brand=jordan")
        let promise = expectation(description: "Completion handler invoked")
        var statusCode: Int?
        var responseError: Error?
        
        // when
        let dataTask = sessionUnderTest?.dataTask(with: url!) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            promise.fulfill()
        }
        dataTask?.resume()
        wait(for: [promise], timeout: 5)
        
        // then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }
    
    func testCallforNikeLocations() {
        // given
        let url =
            URL(string: "https://maps.googleapis.com/maps/api/place/textsearch/json?query=nike&key=AIzaSyAw0m3prCKzqP-zrWauU7DsXJgMDnbQY-Y&location=51.50342345,-0.1195997&radius=25000")
        let promise = expectation(description: "Completion handler invoked")
        var statusCode: Int?
        var responseError: Error?
        
        // when
        let dataTask = sessionUnderTest?.dataTask(with: url!) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            promise.fulfill()
        }
        dataTask?.resume()
        wait(for: [promise], timeout: 5)
        
        // then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }
    
    func testQueryingBooksByAuthor() {
        
        let getAlltheShoes = model?.sections
        XCTAssertEqual(1, getAlltheShoes)
        
    }

    

    
    func testAmountOfShoes(){
       let amountOfShoesReturn = model?.limit
        XCTAssertEqual(String(100), amountOfShoesReturn)
        
    }
    
    
    
    
    
    
    
    
    
    
    
}
