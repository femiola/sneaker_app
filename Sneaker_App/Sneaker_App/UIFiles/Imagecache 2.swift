//
//  Imagecache.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 06/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation
import UIKit

class Imagecach {
    
    private init() {}
    static let shared = Imagecach()
    lazy var imageCache = NSCache<NSString, UIImage>()
    
}

extension UIImageView {
    
    func imageFromURL(_ url: String, placeHolder: UIImage?) {
        let cache = Imagecach.shared
        self.image = nil
        
        if let cachedImage = cache.imageCache.object(forKey: url as NSString) {
            self.image = cachedImage
            return
        }
        
        guard let myurl = URL(string: url) else { return }
        
        URLSession.shared.dataTask(with: myurl, completionHandler: { (data, response, error) in
            if error != nil {
                print("ERROR LOADING IMAGES FROM URL: \(String(describing: error))")
                DispatchQueue.main.async {
                    self.image = placeHolder
                }
                return
            }
            DispatchQueue.global().async {
                if let data = data {
                    if let downloadedImage = UIImage(data: data) {
                        DispatchQueue.main.async {
                            cache.imageCache.setObject(downloadedImage, forKey: url as NSString)
                            self.image = downloadedImage
                        }
                    }
                }
            }
        }).resume()
    }
}
