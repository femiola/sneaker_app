//
//  LoadingView.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 08/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation
import GradientCircularProgress

public struct MyStyle : StyleProperty {
    /*** style properties **********************************************************************************/
    
    // Progress Size
    public var progressSize: CGFloat = 200
    
    // Gradient Circular
    public var arcLineWidth: CGFloat = 10.0
    public var startArcColor: UIColor = UIColor.clear
    public var endArcColor: UIColor = UIColor.black
    
    // Base Circular
    public var baseLineWidth: CGFloat? = 12.0
    public var baseArcColor: UIColor? = UIColor.red
    
    // Ratio
    public var ratioLabelFont: UIFont? = UIFont(name: "Verdana-Bold", size: 16.0)
    public var ratioLabelFontColor: UIColor? = UIColor.white
    
    // Message
    public var messageLabelFont: UIFont? = UIFont.systemFont(ofSize: 16.0)
    public var messageLabelFontColor: UIColor? = UIColor.white
    
    // Background
    public var backgroundStyle: BackgroundStyles = .dark
    
    // Dismiss
    public var dismissTimeInterval: Double? = 1.0 // 'nil' for default setting.
    
    /*** style properties **********************************************************************************/
    
    public init() {}
}
