//
//  SearchStoreTableViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 09/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import MapKit


protocol LocationSelectionDelegate: class {
    func setRegion(_ region: MKCoordinateRegion)
    func nikeStoresSearched(_ model: LocationModel)
}


class SearchStoreTableViewController: UITableViewController {
    
    let geocoder = CLGeocoder()
    var model:LocationModel!
    var location: nikeLoaction!
    var storeRegion: MKCoordinateRegion!
   
    private let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        model = LocationModel()
        tableView.tableFooterView = UIView()
        setupSearchBar()
        
    }
    
    
    // Geocode Function async
    private func forwardGeoCode(_ location: String) {
        geocoder.geocodeAddressString(location) { (placemarks, error) in
            if let error = error {
                print(error)
                
            } else if let placemarks = placemarks {
                print("Placemarks: \(placemarks)")
                if let geoLocation = placemarks.last?.location {
                    let location = LocationComponents(lat: geoLocation.coordinate.latitude, lng: geoLocation.coordinate.longitude)
               
                    self.model.search( location ,completion: {DispatchQueue.main.async {self.tableView.reloadData()
                        }})

                    let region = MKCoordinateRegion(center: geoLocation.coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
                    
                    self.delegate?.setRegion(region)
                    self.delegate?.nikeStoresSearched(self.model)
                }
            }
        }
    }
    
    private func setupSearchBar() {
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Town/Postcode"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
 
    weak var delegate: LocationSelectionDelegate?

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return model.sections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return model.numberOfRowsInSection(section)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "storeLocation", for: indexPath)
        guard let myValue = model.location(at: indexPath) else {return cell}
        
        cell.textLabel?.text = myValue.name
        
        cell.detailTextLabel?.text = myValue.address
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        location = model.location(at: indexPath)
        
        // Get the location
        let temp = CLLocationCoordinate2D(latitude: location.geometry.location.lat, longitude: location.geometry.location.lng)
        // And show the region 
        storeRegion = MKCoordinateRegion(center: temp, latitudinalMeters: 200, longitudinalMeters: 200)
        
        performSegue(withIdentifier: "location", sender: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let showLocationOnTheMap = segue.destination as? StoreDetailViewController{
            showLocationOnTheMap.getLocation = location
            showLocationOnTheMap.region = storeRegion
        }
    }
}





extension SearchStoreTableViewController: UISearchBarDelegate {
    
    // Search bar function to search for repo
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let search = searchBar.text {
            forwardGeoCode(search)
        }
    }
}


