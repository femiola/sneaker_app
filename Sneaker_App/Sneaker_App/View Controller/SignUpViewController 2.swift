//
//  SignUpViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 04/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseFirestore

@available(iOS 13.0, *)
class SignUpViewController: UIViewController {

    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpElements()
    }
    
    func setUpElements(){
        //Hide the error label
        errorLabel.alpha = 0
        Utilities.styleTextField(firstNameTextField)
        Utilities.styleTextField(lastNameTextField)
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(signUpButton)
    }
    



    // Check the fields and validate that the data is correct. If everything is correct, this method
    // returns nil. Otherwise, it returns the error message
    
    func validateFields() -> String?{
        
        // Check that all fields are filled in
        
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" || lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            return "Please fill in all fields"
        }
        
    
        
        // Check if the password is secure
        let cleanedPassword = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let cleanedEmail = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
       if Utilities.isPasswordValid(cleanedPassword) == false {
            return "Please make sure your password is at least 8 characters, contains a special character and a number"
        }
        if Utilities.isEmailValid(cleanedEmail) == false{
            return "Please Enter a valid email address"
        }
        return nil
        
    }
    
    
    @IBAction func signUpPressed() {
        
        //Validate the fields
        
        let error = validateFields()
        
        if error != nil{
            
            // Theres something wrong with the fields, show erroe message
            showError(error!)
        }
        else{
            
            //Create cleaned version of the data
            guard let firstName = firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {return}
            guard let lastName = lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else{return}
            guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {return}
            guard let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {return}
            
            //Create the user
            Auth.auth().createUser(withEmail: email, password: password) {
                (result, err) in
                // Check for errors
                if  err != nil {
                    // There was an error createing the user
                    self.showError("Error creating user")
                }
                else{
                    // User was created successfully, now store the first name and last name
                    let db = Firestore.firestore()
                    
                    db.collection("users").addDocument(data: ["firstname" : firstName , "lastName": lastName, "uid": result?.user.uid ?? "" ]) { (error) in
                        if error != nil{
                            self.showError("Error saving user data")
                        }
                    }
                     //Transition to the home screen
                    self.transitionToHome()
                }
                
            }
        }
    }
    
    func showError(_ message: String){
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
    func transitionToHome() {
      self.performSegue(withIdentifier: "signHome", sender: nil)
    }
}
