//
//  ShoeDetailViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 08/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import SDWebImage


class ShoeDetailViewController: UIViewController {
    
    var selectedShoeDetail: Result!
    var showShoeOnWebView :String = ""

    
    @IBOutlet weak var shoeImageView: UIImageView!
    
    @IBOutlet weak var titelLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var colourWayLabel: UILabel!
    
    @IBOutlet weak var styleIDLabel: UILabel!
    
    @IBOutlet weak var scrollShoes: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shoeImageView.sd_setImage(with: URL(string: selectedShoeDetail.media.imageURL), placeholderImage: #imageLiteral(resourceName: "HyperLogo"))
        
        titelLabel.text = selectedShoeDetail.title
        
        descriptionLabel.text = "Find out how to get your hands on the \(selectedShoeDetail.title) by clicking on the link to below. Showing you the best sneaker from across the UK and Europe"
        descriptionLabel.sizeToFit()
        
        colourWayLabel.text = ("Colourway: \(selectedShoeDetail.colorway)")
        styleIDLabel.text = ("Style Code: \(String(describing: selectedShoeDetail.styleID))")
        
    }
    
    
    @IBAction func tapImage(_ sender: UITapGestureRecognizer) {

        performSegue(withIdentifier: "tappedImage", sender: nil)

    }

    @IBAction func goToWebsite(_ sender: Any) {
        self.showShoeOnWebView = selectedShoeDetail.title
        performSegue(withIdentifier: "goToWebView", sender: nil)
    }
    
    @IBAction func shareTheShoe(_ sender: Any) {
        
        let activitycontroller = UIActivityViewController(activityItems: [titelLabel.text ?? "", selectedShoeDetail.media.imageURL], applicationActivities: nil)
        present(activitycontroller, animated: true, completion: nil)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let goToTheWeb = segue.destination as? WebViewViewController{
            goToTheWeb.viewAllShoes = showShoeOnWebView
        }
        if let taped = segue.destination as? ImageViewController{
            taped.showTappedShoe = selectedShoeDetail
        }
    }
}
