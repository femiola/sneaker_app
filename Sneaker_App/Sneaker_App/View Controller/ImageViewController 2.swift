//
//  ImageViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 11/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import SDWebImage

class ImageViewController: UIViewController {

    
    @IBOutlet weak var selectedTappedImage: UIImageView!
    var showTappedShoe:Result!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        selectedTappedImage.sd_setImage(with: URL(string: showTappedShoe.media.imageURL), placeholderImage: #imageLiteral(resourceName: "HyperLogo"))

    }

    @IBAction func zoomImage(_ sender: UIPinchGestureRecognizer) {
          selectedTappedImage.transform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)  
    }
    
}
