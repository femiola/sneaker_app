//
//  ShoesCollectionViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 06/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import SDWebImage
import GradientCircularProgress
import CoreData
import UserNotifications

private let reuseIdentifier = "shoeCell"




class ShoesCollectionViewController: UICollectionViewController {
    
    
    
    //let coreData: CoreDataContoller! = nil
    var selectItem: Favourite!
    var model: ShoesModel!
    var selectedShoe: Result!
    var progress: GradientCircularProgress!
    var isButtonPress : Bool = false
    private let notificationReminder = NotificationPublisher()
    var reminderSetTime = "00:00:00"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model = ShoesModel()
        
        showAllListOfTrendingShoes()
        
        progress = GradientCircularProgress()
        
        progress.show(message: "Loading", style: MyStyle())
        
        self.progress.dismiss()
        
        
    }
    
    
    func showAllListOfTrendingShoes() {
        
        model.getAllShoes(completion: {DispatchQueue.main.async {
            self.collectionView.reloadData()
            }})
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.hidesBackButton = true
    }
    
    @IBAction func favouriteShoePressed(_ sender: UIButton) {
        
        isButtonPress.toggle()
        
        if let cell = sender.superview?.superview as? shoeCollection {
            if let indexPath = self.collectionView.indexPath(for: cell) {
                
                if let selectedFavShoe = model.allShoes(at: indexPath) {
                    let favourit = Favourite(context: CoreDataContoller.shared.mainContext)
                    favourit.name = selectedFavShoe.title
                    favourit.image = selectedFavShoe.media.imageURL
                    saveAndDeleteToFav(name: favourit)
                    
                }
            }
        }
    }
    
    // Trying to save and delete however its saving not deleting still working on it
    func saveAndDeleteToFav(name: Favourite){
        if isButtonPress == true{
            
            CoreDataContoller.shared.saveContext()
        }
        else if isButtonPress == false
        {
            print("It Should delete")
            print(name)
            CoreDataContoller.shared.mainContext.delete(name)
        }
    }
    
    
    @IBAction func reminder(_ sender: UIButton) {
        
        isButtonPress.toggle()
        if isButtonPress{
            //  setting the reminder on the shoe the user choses
            if let cell = sender.superview?.superview as? shoeCollection {
                if let indexPath = self.collectionView.indexPath(for: cell) {
                    
                    if let selectedReminder = model.allShoes(at: indexPath) {
                        
                        //Setting the notification by specifying what to say when the notification is called
                        notificationReminder.sendNotification(title: "Heyyy", subtitle: "Dont forget this HYPED shoes", body: "\(selectedReminder.title) ", badge: 1)
                    }
                }
            }
        }
        else{
            // When the button is press it will not recieve another notification
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["taskreminder"])
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return model.sections
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return model.numberOfRowsInSection(section)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let shoeInfocell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? shoeCollection else { return shoeInfocell }
        
        cell.layer.cornerRadius = 7.5
        
        if let allSneakers = model.allShoes(at: indexPath){
            cell.shoeInforLabel.text = allSneakers.title
            cell.shoeImageView.sd_setImage(with: URL(string: allSneakers.media.imageURL), placeholderImage: #imageLiteral(resourceName: "icon"))
            
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedSneaker = model.allShoes(at: indexPath) else {return print("Nothing Selected")}
        
        selectedShoe = selectedSneaker
        
        performSegue(withIdentifier: "shoeDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let shoeInformation = segue.destination as? ShoeDetailViewController{
            shoeInformation.selectedShoeDetail = selectedShoe
        }
    }
}

class shoeCollection: UICollectionViewCell {
    @IBOutlet weak var shoeImageView: UIImageView!
    
    @IBOutlet weak var shoeInforLabel: UILabel!
}

