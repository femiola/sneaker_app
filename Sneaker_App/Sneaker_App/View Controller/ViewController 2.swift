//
//  ViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 04/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class ViewController: UIViewController {


    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        setUpElements()
    }
    
    func setUpElements(){
        Utilities.styleFilledButton(signUpButton)
        Utilities.styleHollowButton(loginButton)
    }
    

    @IBAction func homeUnWind(unwindSegue: UIStoryboardSegue){
        
    }

    
    //Function for Dark Mode
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        guard (previousTraitCollection?.hasDifferentColorAppearance(comparedTo: traitCollection)) != nil else {return }
    }

}

