//
//  StoreDetailViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 09/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import MapKit

class StoreDetailViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    var model: LocationModel!
    var region: MKCoordinateRegion!
    var lastAnnot: MKPointAnnotation!
    var getLocation: nikeLoaction!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        updateRegion()
        updateMap(getLocation.geometry.location.lat, lon: getLocation.geometry.location.lng)
        
    }
    
    func updateRegion(){
        mapView.setRegion(region, animated: true)
    }
    
    func updateMap(_ lat: Double, lon: Double){
        
        let annotation = MKPointAnnotation()
        annotation.title = getLocation.name
        annotation.subtitle = getLocation.address
        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        let pin = CustomAnnotation(coordinate: annotation.coordinate, title: annotation.title ?? "", subtitle: annotation.subtitle ?? "")
        print(pin)
        mapView.addAnnotation(pin)
        
    }
    
    class CustomAnnotation: NSObject, MKAnnotation {
        var coordinate: CLLocationCoordinate2D
        var title: String?
        var subtitle: String?
        
        init(coordinate: CLLocationCoordinate2D, title: String = "Title", subtitle: String = "Subtitle") {
            
            self.coordinate = coordinate
            self.title = title
            self.subtitle = subtitle
        }
    }
}

extension StoreDetailViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let Identifier = "pin"
        let annotaView = mapView.dequeueReusableAnnotationView(withIdentifier: Identifier) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: Identifier)
  
        annotaView.canShowCallout = true
        annotaView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        if annotation is MKUserLocation {
            return nil
        } else if annotation is CustomAnnotation {
            annotaView.image =  #imageLiteral(resourceName: "map 2x")
            return annotaView
        } else {
            return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        
        if control == view.rightCalloutAccessoryView{
            guard let addressName = view.annotation?.title else {return}
            guard let addressDetail = view.annotation?.subtitle else {return}
            
            
            let ac = UIAlertController(title: addressName, message: addressDetail, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "Ok", style: .default))
            present(ac,animated: true)
        }
    }
}










