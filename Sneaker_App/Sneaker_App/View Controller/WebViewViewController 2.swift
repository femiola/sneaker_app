//
//  WebViewViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 07/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController {
    
    @IBOutlet weak var myWebView: WKWebView!
    var viewAllShoes: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myWebView.navigationDelegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let searchString = viewAllShoes.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)
        {
            // create the URL string
            let urlString = "https://www.google.com/search?q=\(searchString)"
            
            if let url = URL(string: urlString){
                myWebView.load(URLRequest(url: url))
            }
        }
    }
}


extension WebViewViewController: WKNavigationDelegate{
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let trust = challenge.protectionSpace.serverTrust,SecTrustGetCertificateCount(trust) > 0 {
            completionHandler(.useCredential, URLCredential(trust: trust))
            return
        }
        completionHandler(.cancelAuthenticationChallenge, nil)
    }
}

