//
//  ShoesCollectionViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 06/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import SDWebImage
import GradientCircularProgress

private let reuseIdentifier = "shoeCell"

class ShoesCollectionViewController: UICollectionViewController {
    
    var model: ShoesModel!
    var selectedShoe: Result!
    var progress: GradientCircularProgress!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        model = ShoesModel()
        
        showAllListOfTrendingShoes()
        
        progress = GradientCircularProgress()
        
        progress.show(message: "Loading", style: MyStyle())
        self.progress.dismiss()
        
    }
    
    
    func showAllListOfTrendingShoes() {
        
        model.getAllShoes(completion: {DispatchQueue.main.async {
            self.collectionView.reloadData()
            }})
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.navigationItem.hidesBackButton = true
    }
    
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return model.sections
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return model.numberOfRowsInSection(section)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let shoeInfocell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? shoeCollection else { return shoeInfocell }
        
        cell.layer.cornerRadius = 7.5
        
        if let allSneakers = model.allShoes(at: indexPath){
            cell.shoeInforLabel.text = allSneakers.title
            cell.shoeImageView.sd_setImage(with: URL(string: allSneakers.media.imageURL), placeholderImage: #imageLiteral(resourceName: "HyperLogo"))
            
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedSneaker = model.allShoes(at: indexPath) else {return print("Nothing Selected")}
        
        selectedShoe = selectedSneaker
        
        performSegue(withIdentifier: "shoeDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let shoeInformation = segue.destination as? ShoeDetailViewController{
            shoeInformation.selectedShoeDetail = selectedShoe
        }
    }
}

class shoeCollection: UICollectionViewCell {
    @IBOutlet weak var shoeImageView: UIImageView!
    
    @IBOutlet weak var shoeInforLabel: UILabel!
}

