//
//  ShoeDetailViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 08/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import SDWebImage
import CoreData
import UserNotifications



class ShoeDetailViewController: UIViewController {
    
    var selectItem: Favourite!
    var selectedShoeDetail: Result!
    var showShoeOnWebView :String = ""
    var isButtonPress : Bool = false
    private let notificationReminder = NotificationPublisher()
    
    
    
    @IBOutlet weak var shoeImageView: UIImageView!
    
    @IBOutlet weak var titelLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var colourWayLabel: UILabel!
    
    @IBOutlet weak var styleIDLabel: UILabel!
    
    @IBOutlet weak var scrollShoes: UIScrollView!
    
    @IBOutlet weak var reminderButton: UIButton!
    
    @IBOutlet weak var favouriteButton: UIButton!
    
    @IBOutlet weak var goToWeb: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shoeImageView.sd_setImage(with: URL(string: selectedShoeDetail.media.imageURL), placeholderImage: #imageLiteral(resourceName: "HyperLogo"))
        
        titelLabel.text = selectedShoeDetail.title
        
        descriptionLabel.text = "The \(selectedShoeDetail.title) has nevere been a subtle sneaker. This is one of HYPER upcoming sneaker to find out more click on the  button below. As we would show you the best sneakers across Europe."
        
        descriptionLabel.sizeToFit()
        
        colourWayLabel.text = ("Colourway: \(selectedShoeDetail.colorway)")
        styleIDLabel.text = ("Style Code: \(String(describing: selectedShoeDetail.styleID))")
        
        setUpElements()
        
    }
    
    func setUpElements(){
        Utilities.styleFilledButton(goToWeb)
    }
    
    
    @IBAction func saveToFavourite(_ sender: UIButton) {
        
        isButtonPress.toggle()
        
        let favourit = Favourite(context: CoreDataContoller.shared.mainContext)
        
        
        
        if isButtonPress{
            favouriteButton.backgroundColor = .red
            
            favourit.id = selectedShoeDetail.id
            favourit.name = selectedShoeDetail.title
            favourit.image = selectedShoeDetail.media.imageURL
            CoreDataContoller.shared.saveContext()
            
            favouriteButton.tag = 2
        } else {
            favouriteButton.backgroundColor = .none
            favouriteButton.tag = 1
            deleteFromCoreData(id: favourit.id ?? "")
        }
    }

    func deleteFromCoreData(id: String) -> Void {
        let getContext = CoreDataContoller.shared.mainContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Favourite")
        let predicate = NSPredicate(format: "id == %d", argumentArray: [id])
        fetchRequest.predicate = predicate
        
        let moc = getContext
        let result = try? moc.fetch(fetchRequest)
        let resultData = result as! [Favourite]
        
        for object in resultData {
            moc.delete(object)
        }
        
        do {
            try moc.save()
            print("saved!")
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
    }
    
    
    @IBAction func tapImage(_ sender: UITapGestureRecognizer) {
        
        performSegue(withIdentifier: "tappedImage", sender: nil)
        
    }
    @IBAction func reminder(_ sender: Any) {
     
        isButtonPress.toggle()
        if isButtonPress{
            reminderButton.backgroundColor = .red
            notificationReminder.sendNotification(title: "Heyyy", subtitle: "Dont for get this HYPED shoes", body: "\(selectedShoeDetail.title) ", badge: 0)
        } else {
            reminderButton.backgroundColor = .none
            print("this happend")
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["reminder"])
        }
    }
    
    @IBAction func goToWebsite(_ sender: Any) {
        
        self.showShoeOnWebView = selectedShoeDetail.title
        performSegue(withIdentifier: "goToWebView", sender: nil)
    }
    
    @IBAction func shareTheShoe(_ sender: Any) {
        
        let activitycontroller = UIActivityViewController(activityItems: [titelLabel.text ?? "", selectedShoeDetail.media.imageURL], applicationActivities: nil)
        present(activitycontroller, animated: true, completion: nil)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let goToTheWeb = segue.destination as? WebViewViewController{
            goToTheWeb.viewAllShoes = showShoeOnWebView
        }
        if let taped = segue.destination as? ImageViewController{
            taped.showTappedShoe = selectedShoeDetail
        }
    }
}
