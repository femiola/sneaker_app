//
//  LoginViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 04/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import FirebaseAuth

@available(iOS 13.0, *)
class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setUpElements()
    }
    
    func setUpElements(){
        
        Utilities.styleTextField(emailTextField)
        Utilities.styleTextField(passwordTextField)
        Utilities.styleFilledButton(loginButton)
    }
    
    @IBAction func loginPressed() {
        
        // Validate Text Fields
        guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {return}
        guard let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {return}
        
        // Create cleaned versions of the text field
        
        Auth.auth().signIn(withEmail: email, password: password) {
            (result, error) in
            
            guard error == nil else{
                
                let alert = UIAlertController(title: "Invalid login", message: error?.localizedDescription, preferredStyle: .alert)
                alert.addAction(.init(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
                return
            }
            self.performSegue(withIdentifier: "signIn", sender: nil)
        }
    }
}
