//
//  HomeViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 04/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import Firebase


@available(iOS 13.0, *)
class signOutViewController: UIViewController {

    @IBOutlet weak var signOutButton: UIButton!
    
    @IBOutlet weak var userName: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let username = Auth.auth().currentUser?.email else{return}
        
        userName.text = "Hello \(username)"
        
        setUpElements()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func setUpElements(){
        Utilities.styleFilledButton(signOutButton)
    }


    
    @IBAction func signOut(_ sender: Any) {
    // Signout function by using the firebase authentication signout 
          let firebaseAuth = Auth.auth()
        do {
          try firebaseAuth.signOut()
             self.performSegue(withIdentifier: "goHome", sender: self)
            
        } catch let signOutError as NSError {
            
          print ("Error signing out: %@", signOutError)
        }
    }
    


  
}

