//
//  SearchBrandTableViewController.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 07/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import UIKit
import SDWebImage
import GradientCircularProgress


class CustomTableViewCell : UITableViewCell{
    
    @IBOutlet weak var shoeImageView: UIImageView!
    
    @IBOutlet weak var brandNameLabel: UILabel!
    
    @IBOutlet weak var genderLabel: UILabel!
    
    @IBOutlet weak var retailPriceLabel: UILabel!
    
    @IBOutlet weak var releaseDateLabel: UILabel!
}

class SearchBrandTableViewController: UITableViewController {

    var model: SearchSneakersModel!
     var selectedBrandShoe: Result!
     var progress: GradientCircularProgress!
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model = SearchSneakersModel()
        
        tableView.tableFooterView = UIView()
        setupSearchBar()
        
       //setupTableViewBackgroundView()
        
        progress = GradientCircularProgress()

    }
    
    private func setupSearchBar() {
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Brand"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    // Background view function
    private func setupTableViewBackgroundView() {
        let backgroundView = UILabel(frame: .infinite)
        backgroundView.textColor = .purple
        backgroundView.numberOfLines = 0
        backgroundView.text = "No Results to search for your favorite sneakers enter them in the search box above"
        tableView.backgroundView = backgroundView
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return model.sections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return model.numberOfRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchShoe = tableView.dequeueReusableCell(withIdentifier: "whatShoes", for: indexPath)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "whatShoes", for: indexPath) as? CustomTableViewCell else{ return searchShoe}
        
        if let shoeBrands = model.allShoes(at: indexPath){

            cell.shoeImageView.sd_setImage(with: URL(string: shoeBrands.media.thumbURL), placeholderImage: #imageLiteral(resourceName: "Jordan11Logo"))

            cell.brandNameLabel.text = shoeBrands.title
            cell.genderLabel.text = shoeBrands.gender.rawValue

            if let shoeRetailPrice = shoeBrands.retailPrice {
                cell.retailPriceLabel.text = String(shoeRetailPrice)
            }else{
                cell.retailPriceLabel.text = "N/A"
            }
            
            if let shoeReleaseDate = shoeBrands.releaseDate {
                cell.releaseDateLabel.text = String(shoeReleaseDate)
                print(shoeReleaseDate)
            }else{
                cell.releaseDateLabel.text = "N/A"
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         guard let selectedBrandShoes = model.allShoes(at: indexPath) else {return print("Nothing Selected")}
                selectedBrandShoe = selectedBrandShoes
                performSegue(withIdentifier: "findOutMore", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let shoeInformation = segue.destination as? ShoeDetailViewController{
            shoeInformation.selectedShoeDetail = selectedBrandShoe
        }
    }
}

extension SearchBrandTableViewController: UISearchBarDelegate {
    
    // Search bar function to search for shoes brand
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if let search = searchBar.text {
            progress.show(message: "Loading", style: MyStyle())
            self.progress.dismiss()
            model.searchAllShoes(search, completion: {DispatchQueue.main.async {
            self.tableView.reloadData()
            }})
        }
    }
}



