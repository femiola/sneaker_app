//
//  SearchShoesModel.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 07/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation


class SearchSneakersModel{
    
    var SearchAllSneakers = [Result]()
    
    var shoeAmount: String = "100"
    
    func searchAllShoes(_ searchBrand: String, completion: @escaping () -> Void) {
        
        var urlComponents = URLComponents()
       
        urlComponents.scheme = "https"
        urlComponents.host  = "api.thesneakerdatabase.com"
        urlComponents.path = "/v1/sneakers"
        urlComponents.queryItems = [URLQueryItem(name: "limit", value: shoeAmount),
        URLQueryItem(name: "name", value: searchBrand)
        ]
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        print(url)
        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
               
                completion()
                
                
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    func parseJSON(_ data : Data) {
            let decoder = JSONDecoder()
            do {
                let decodedData = try decoder.decode(RootSearchResult.self, from: data)
                
                self.SearchAllSneakers = decodedData.results
            }
            catch {
                print("this is the pass error\(error)")
            }
        }
    }





extension SearchSneakersModel{
    
    var sections: Int{
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return SearchAllSneakers.count
    }
    
    func allShoes(at indexPath: IndexPath) -> Result? {
        if indexPath.section < 0 || indexPath.section > sections{
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection(indexPath.section){
            return nil
        }
        return SearchAllSneakers[indexPath.row]
    }
}
