//
//  Model.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 06/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation



// MARK: - RootSearchResult
struct RootSearchResult: Codable {
    let count : Int
    let results: [Result]
    
    enum CodingKeys: String, CodingKey {
        case count, results
    }
}

// MARK: - Result
struct Result: Codable {
    let id: String
    let brand: String
    let colorway: String
    let gender: Gender
    let media: Media
    let releaseDate: String?
    let retailPrice: Int?
    let styleID: String?
    let title: String
    let year: Int?

    enum CodingKeys: String, CodingKey {
        case id, brand, colorway, gender,media, releaseDate, retailPrice
        case styleID = "styleId"
        case title, year
    }
}



enum Gender: String, Codable {
    case child = "child"
    case infant = "infant"
    case men = "men"
    case preschool = "preschool"
    case toddler = "toddler"
    case women = "women"
}

// MARK: - Media
struct Media: Codable {
    let imageURL, smallImageURL, thumbURL: String

    enum CodingKeys: String, CodingKey {
        case imageURL = "imageUrl"
        case smallImageURL = "smallImageUrl"
        case thumbURL = "thumbUrl"
    }
}
