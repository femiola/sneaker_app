//
//  MapLocation.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 09/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation

// MARK: - MapRootObject
struct MapRootObject:Codable {
    var results: [nikeLoaction]
    
}

// MARK: - NikeLocation
struct nikeLoaction : Codable {
    var geometry: Location
    var address: String
    var name: String
   
    
        enum CodingKeys: String, CodingKey {
        case geometry
        case address = "formatted_address"
        case name
    }
}

// MARK: - Location
struct Location: Codable {
    var location: LocationComponents
}

struct LocationComponents: Codable {
    var lat: Double
    var lng: Double
}
