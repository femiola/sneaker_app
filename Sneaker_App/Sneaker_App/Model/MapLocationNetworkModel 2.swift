//
//  MapLocationNetworkModel.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 09/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation


class LocationModel {
    
    var myLocation = [nikeLoaction]()
    
    func search( _ searchLocation: LocationComponents, completion: @escaping () -> Void) {
        
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host  = "maps.googleapis.com"
        urlComponents.path = "/maps/api/place/textsearch/json"
        
        urlComponents.queryItems = [URLQueryItem(name: "query", value: "nike"),
                                    URLQueryItem(name: "key", value: "AIzaSyAw0m3prCKzqP-zrWauU7DsXJgMDnbQY-Y"),
                                    URLQueryItem(name: "location", value: "\(searchLocation.lat),\(searchLocation.lng)"),
                                    URLQueryItem(name: "radius", value: "25000")]
        
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
                completion()
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    func parseJSON(_ data : Data) {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(MapRootObject.self, from: data)
            self.myLocation = decodedData.results
        }
        catch {
            print(error)
        }
    }
}


extension LocationModel{
    var sections: Int{
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return myLocation.count
    }
    
    func location(at indexPath: IndexPath) -> nikeLoaction? {
        if indexPath.section < 0 || indexPath.section > sections{
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection(indexPath.section){
            return nil
        }
        return myLocation[indexPath.row]
    }
}
