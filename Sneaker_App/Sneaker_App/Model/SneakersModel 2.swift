//
//  SneakersModel.swift
//  Sneaker_App
//
//  Created by Femi Oladiji on 06/04/2020.
//  Copyright © 2020 Femi Oladiji. All rights reserved.
//

import Foundation

class ShoesModel{
    
    var getAllSneakers = [Result]()
    
    var limit: String = "100"
    
    func getAllShoes(completion: @escaping () -> Void) {
        
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host  = "api.thesneakerdatabase.com"
        urlComponents.path = "/v1/sneakers"
        urlComponents.queryItems = [URLQueryItem(name: "limit", value: limit)]
        
        guard let url = urlComponents.url else { return print("Unable to create URL \(urlComponents)")}
        
        let dataTask = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            if let error = error{
                print("Error - \(error.localizedDescription)")
            } else if let data = data{
                self.parseJSON(data)
                completion()
            }else{
                print("This should never happen")
            }
        }
        dataTask.resume()
    }
    
    func parseJSON(_ data : Data) {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(RootSearchResult.self, from: data)
            self.getAllSneakers = decodedData.results
        }
        catch {
            print("this is the pass error\(error)")
        }
    }
}





extension ShoesModel{
    var sections: Int{
        return 1
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return getAllSneakers.count
    }
    
    func allShoes(at indexPath: IndexPath) -> Result? {
        if indexPath.section < 0 || indexPath.section > sections{
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection(indexPath.section){
            return nil
        }
        return getAllSneakers[indexPath.row]
    }
}

